#!/bin/bash

# Add User
useradd ${USERNAME}

# generate psuedo-random password from USERNAME
password=$(echo "${USERNAME}" | md5sum | base64 | cut -c1-10)

# Set password
echo "${password}" | passwd --stdin ${USERNAME}

# Add user to sudoers file
echo "${USERNAME} ALL=(ALL) ALL" > /etc/sudoers.d/${USERNAME}

# Display username and password in logs
echo "Username: ${USERNAME}"
echo "Password: ${password}"

# mv ssh configs backup to default location and remove .BAK directory
/usr/bin/cp -a /etc/ssh.BAK/* /etc/ssh/
rm -rf /etc/ssh.BAK/

# spawn supervisord
/usr/bin/supervisord -c /etc/supervisord.conf
