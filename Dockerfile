FROM fedora:latest

ARG NAME="ctf-base-ssh"
ARG DESCRIPTION="A container that runs SSH for us in the c2games CTF"
ARG MAINTAINER="Lucas Halbert <lhalbert@c2games.org>"
ARG VENDOR=c2games
ARG ARCH=amd64
ARG VCS_URL=https://gitlab.com/c2-games/ctf/base-ssh-container
ARG DOCKER_CMD="docker run -d -p 2222:22 --rm registry.gitlab.com/c2-games/ctf/base-ssh-container"
ARG IMAGE_USAGE="https://gitlab.com/c2-games/ctf/base-ssh-container/-/blob/main/README.md"
ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

MAINTAINER Lucas Halbert <lhalbert@c2games.org>
LABEL maintainer="${MAINTAINER}" \
      org.label-schema.maintainer="${MAINTAINER}" \
      org.label-schema.build-date=${BUILD_DATE} \
      org.label-schema.name="${NAME}" \
      org.label-schema.vendor="${VENDOR}" \
      org.label-schema.description="${DESCRIPTION}" \
      org.label-schema.version=${VERSION} \
      org.label-schema.architecture=${ARCH} \
      org.label-schema.vcs-ref=${VCS_REF} \
      org.label-schema.vcs-url=${VCS_URL} \
      org.label-schema.docker.cmd=${DOCKER_CMD} \
      org.label-schema.usage=${IMAGE_USAGE} \
      org.label-schema.schema-version="1.0"

ENV USERNAME=team0

RUN dnf makecache \
    && dnf install -y openssh-server openssh-clients rng-tools supervisor procps vim nano iproute strace gdb binutils lsof curl wget coreutils passwd \
    && dnf update -y \
    && dnf clean all \
    && mkdir -p /root/.ssh/ /var/run/sshd/ \
    && ssh-keygen -b 4096 -A \
    && mkdir -p /root/.ssh/ \
    && chmod 700 /root/.ssh/ \
    && cp -a /etc/ssh /etc/ssh.BAK

COPY root/.ssh/authorized_keys /root/.ssh/authorized_keys
COPY docker-entrypoint.sh /usr/bin/
COPY etc/ /etc/


EXPOSE 22

ENTRYPOINT ["docker-entrypoint.sh"]
